# Running Jupyter on a Supercomputer
Copy from : https://gist.githubusercontent.com/mcburton/d80e4395cd82737d3677c570aa31ee40/raw/f8fda3d52649100f6d4982bace91e587ebd75529/jupyter-on-a-supercomputer.md Adapted for EBI lsf cluster
Go there for the whole text on details. Here is a quick and simple solution.
I assume that you installed jupyter-lab (https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html) and are able to login and start an interactive 
session on the cluster.

```
mkdir ~/runtime
echo 'export JUPYTER_RUNTIME_DIR=~/runtime' >> .bashrc
#export XDG_RUNTIME_DIR="" # not sure if this is needed
source .bashrc
```
### Launch an Interactive Session
```
bsub -M 20000 -R rusage[mem=8192] -Is /usr/local/bin/bash
# get the hostname
hostname
# ebi6-052.ebi.ac.uk
```
## ssh tunnel to compute node
Now we will open an SSH tunnel to access jupyter via your (local) browser.
On the compute node:
```
jupyter-lab --no-browser --ip=0.0.0.0 --port=NNNN
# NNNN chose something like 8888 or 8789 or whatever. chose above 8000
```
Now that we have the hostname we can create an SSH tunnel through the login node to this compute node (step six) and access the Jupyter Notebook server we will launch in the next step.
On your laptop:
```
ssh -L 1111:ebiXXX.ebi.ac.uk:NNNN ebi
```
### Aside: silent ssh
As an aside, the above command will log you into the head node. You can avoid logging into head node, and also run the tunnelling in the background, by using -N and -f flags respectively:
```
ssh -N -f -L 1111:ebiXXX.ebi.ac.uk:NNNN ebi
```
Now when you want to kill the process:
```
lsof -i tcp:1111 # Find the 'PID' of ssh process
kill -9 <PID>
```
(Source: https://oncomputingwell.princeton.edu/2018/05/jupyter-on-the-cluster/ )
## Run!
Open in your browser:
```
localhost:1111
```
