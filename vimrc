""""""Vundle plugin manager""""""

set nocompatible              " be iMproved, required
filetype off                  " required
set backspace=indent,eol,start
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Put Vundle plugins here
" Tmux navigator
Plugin 'christoomey/vim-tmux-navigator'

" Tmux resize
Plugin 'RyanMillerC/better-vim-tmux-resizer'

" YouCompleteMe - IDE-like completion
Plugin 'Valloric/YouCompleteMe'

" Hybrid colorscheme
Plugin 'w0ng/vim-hybrid'

" Nice bottom info line
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" Remove unnecessary menus (gvim)
set guioptions-=T 
set guioptions-=r
set guioptions-=L
set guioptions-=m
set guioptions+=c
set number

" Set colorscheme
set background=dark
colorscheme hybrid

" Turn on syntax highlighting
syntax on

" Color line numbers a little lighter
highlight LineNr ctermfg=lightgrey

"Highlight cursor line and column
set cursorline
set cursorcolumn

" Use Tab and Shist-Tab for switching between buffers
nnoremap <Tab> :bnext<CR>
nnoremap <S-Tab> :bprevious<CR>

if has('gui_running')
	set guifont=Monaco:h18
endif

"Disable annoying error bells
set noerrorbells visualbell t_vb=
if has('autocmd')
  autocmd GUIEnter * set visualbell t_vb=
endif

" Enable right-click menu
set mousemodel=popup

" Enable mouse scroll
set mouse=a

" Tmux navigator bindings remapping for ssh session
let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <c-Left> :TmuxNavigateLeft<cr>
nnoremap <silent> <c-Down> :TmuxNavigateDown<cr>
nnoremap <silent> <c-Up> :TmuxNavigateUp<cr>
nnoremap <silent> <c-Right> :TmuxNavigateRight<cr>
nnoremap <silent> <c-Tab> :TmuxNavigatePrevious<cr>

" Tmux resizer bindings remapping for ssh session
nnoremap <silent> <M-Left> :TmuxResizeLeft<cr>
nnoremap <silent> <M-Down> :TmuxResizeDown<cr>
nnoremap <silent> <M-Up> :TmuxResizeUp<cr>
nnoremap <silent> <M-Right> :TmuxResizeRight<cr>
