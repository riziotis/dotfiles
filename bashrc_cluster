# ~/.bashrc: executed by bash(1) for non-login shells.

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
#
# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
	#PS1="\[$(tput bold)\]\[\033[38;5;196m\]\u@\[$(tput bold)\]\[\033[38;5;124m\]\h\[$(tput bold)\]\[\033[38;5;15m\]:\[$(tput bold)\]\[\033[38;5;69m\]\w\[$(tput bold)\]\[\033[38;5;15m\]\\$\[$(tput sgr0)\] "
	PS1="\[$(tput bold)\]\[\033[38;5;196m\]\u\[$(tput sgr0)\]@\[$(tput sgr0)\]\[\033[38;5;202m\]\h\[$(tput sgr0)\]:\[$(tput sgr0)\]\[\033[38;5;33m\]\W\[$(tput sgr0)\] "
else
	PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Add default working directory in pythonpath for testing python code
export PYTHONPATH="$PYTHONPATH:/nfs/research/thornton/riziotis/research"


# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias d='ls -lhF -v'
alias 1='ls -1'
alias z='ls -1 | wc -l'

# miscellaneous aliases
alias acroread='xdg-open'
alias gvim='vim'
alias nfs='cd /nfs/research/thornton/riziotis'
alias hps='cd /hps/nobackup/thornton/'
alias cap='conda activate py3'
alias cab='conda activate base'
alias path='readlink -f'
alias cawk='awk -F ","'

# LSF aliases
alias bj='bjobs'
alias nj='bjobs | grep rizioti | wc -l'
alias bj_all="bjobs -u all | awk '{if (NR!=1) {print \$2}}' | sort | uniq -c | sort -rn"
alias bj_rung="bjobs -r -u all | awk '{if (NR!=1) {print \$2}}' | sort | uniq -c | sort -rn"
alias bkill_pend="bjobs | awk '\$3==\"PEND\" {print \$1}' | xargs bkill"
alias blame='busers | head -1 && busers all | tail -n+2 | sort -n -r -k4'

# Start an interactive job on the cluster, where
# - the first argument is GiB of memory (default: 4)
# - the second argument is number of cores (default: 1)
# - the third argument is the queue (default: research, use highpri on yoda)
function sh() {
    bsub -M $(( ${1:-4} * 1024 )) -n ${2:-1} \
        -R "rusage[mem=$(( ${1:-4} * 1024 ))]" \
        -R "rusage[tmp=5000]" \
        -Is -q ${3:-research} "$SHELL"
}
export sh

# Add an "alert" alias for long running commands.  Use like so: sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/hps/software/users/thornton/riziotis/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/hps/software/users/thornton/riziotis/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/hps/software/users/thornton/riziotis/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/hps/software/users/thornton/riziotis/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

# Activate py3 env by default
conda activate py3
export JUPYTER_RUNTIME_DIR=~/jupyter_runtime
